import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      products: {
          weed: [
              {
                  id:1,
                  name: "Orange Juice",
                  description:
                      "With supporting text below as a natural lead-in to additional content.",
                  cbd: "12%",
                  thc: "15%",            
              },
              {
                  id:2,
                  name: "Gorilla Glue",
                  description:
                      "With supporting text below as a natural lead-in to additional content.",
                  cbd: "22%",
                  thc: "35%",
              },
              {
                  id:3,
                  name: "Alien Cookie",
                  description:
                      "With supporting text below as a natural lead-in to additional content.",
                  cbd: "2%",
                  thc: "4%",
              }                
          ]
      }
  },
  mutations: {
  },
  getters: {
    getProductsWeed(state) {
        return state.products.weed;
    },
  },
  actions:{

  },
  modules: {
  }
})
