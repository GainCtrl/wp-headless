const path = require("path");

module.exports = {
  publicPath: '/wp-content/themes/undefined-vuejs/vue-files',
  outputDir: path.resolve(__dirname, "/wp-content/themes/undefined-vuejs/vue-files"),
  runtimeCompiler: true,
  productionSourceMap: false,
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        './src/assets/scss/style.scss'
      ]
    }
  }
}
